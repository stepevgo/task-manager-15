package ru.t1.stepanishchev.tm.exception.system;

public final class ArgumentNotSupportedException extends AbstractSystemException{

    public ArgumentNotSupportedException() {
    }

    public ArgumentNotSupportedException(final String argument) {
        super("Error! Argument '" + argument + "' is not supported.");
    }

}